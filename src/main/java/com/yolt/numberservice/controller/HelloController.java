package com.yolt.numberservice.controller;

import com.yolt.numberservice.controller.dto.Greeting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@Slf4j
public class HelloController {
    @GetMapping
    public Greeting hello() {
        log.info("Hello");
        return new Greeting("Hello World");
    }
}
