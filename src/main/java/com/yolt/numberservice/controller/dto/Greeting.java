package com.yolt.numberservice.controller.dto;

import lombok.Value;

@Value
public class Greeting {
    private String message;
}
